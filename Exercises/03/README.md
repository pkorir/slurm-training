**Exercise #3:** 

**A: Submit a batch job with specific node and CPU requirements**

**Objective:** Submit a batch job requesting specific node and CPU resources and print the hostname.

**Instructions:**
1. Submit a batch job requesting 2 nodes from the "training-oct-23" reservation and "training-oct-23" account.
2. In the batch script, print the hostname of each allocated node.


**B: Submit an sbatch one-liner**

**Instructions:**
1. Using the same job requirements from exercise "A". Submit the job using sbatch one-liner with "--wrap"
