## Job Dependencies
  
### Task 01: Create two SLURM jobs where the second job starts only after the first job finishes successfully

Instructions:

1. Submit the first job that performs some task (i.e sleep for some time).
2. Submit the second job with a dependency on the first job so that it only starts when the first job completes successfully.
3. Observe pending reason for the second job
4. Cancel the first job or wait for it to finish and check if the second job starts

A useful way to link to jobs is to use:

```shell
job_id=$(sbatch --parsable jobA.batch)
sbatch --dependency=afterok:$job_id jobB.batch 
```
