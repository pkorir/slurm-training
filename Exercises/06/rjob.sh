#!/usr/bin/env bash

#SBATCH --job-name=fibonacci-with-r
#SBATCH --output=fibonacci-with-r.out
#SBATCH --error=fibonacci-with-r.err
#SBATCH --time=0-00:05
#SBATCH --mem-per-cpu=2g
#SBATCH --cpus-per-task=1
#SBATCH --nodes=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=pkorir@ebi.ac.uk


module load r/4.3.0
Rscript myapp.R
