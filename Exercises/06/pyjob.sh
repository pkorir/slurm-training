#!/usr/bin/env bash

#SBATCH --job-name=fibonacci-with-python
#SBATCH --output=fibonacci-with-python.out
#SBATCH --error=fibonacci-with-python.err
#SBATCH --time=0-00:05
#SBATCH --mem-per-cpu=2g
#SBATCH --cpus-per-task=1
#SBATCH --nodes=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=pkorir@ebi.ac.uk


module load python/3.10.10
python myapp.py
python myapp.py 10
python myapp.py 20
python myapp.py 100

