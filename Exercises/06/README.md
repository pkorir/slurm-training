## Using Software Modules
  
### Task 01: Write a SLURM Batch job which loads latest version of R and run 'myapp.R' script placed in this directory

Here are few hints to attempt this task

1. Create a slurm-R.job file and add relevant SLURM Headers. Ask for cpus-per-task=2, memory=4GB and walltime=2minutes
2. Identify latest version of R available on Codon Cluster
3. Add `module load r/ver` in your slurm.job. Replace 'ver' with version number you have identified is the latest on Codon. Ask instructor if you are not sure about this.
4. To run R script in batch job, you need add `Rscript myapp.R` at end of your slurm.job file.
5. Submit Batch job by issuing `sbatch slurm-R.job`
6. Inspect the results

### Task 02: Write a Slurm Batch script which uses Anaconda Environment to run a script

Here are few hints to attempt this task

1. Create slurm-anaconda.job and add relevant SLURM Headers. Ask for cpus-per-task=4, memory=12GB and walltime=1minute
2. Add `source /hps/software/opt/linux-rocky8-cascadelake/anaconda3/etc/profile.d/conda.sh` to enable Conda commands. For real workload, you should replace this with your Project Anaconda Installation path.
3. Now activate conda environment by adding `conda activate base` line in your job script.
4. Add `python3 myapp.py` line at the end of Job script to run python script.
5. Inspect output
