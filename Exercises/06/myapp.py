import argparse
import sys
import unittest


class Test(unittest.TestCase):
    def test(self):
        sys.argv = ['myapp.py', '1']
        main()
        self.assertEqual(1, 1)

    def test_no_args(self):
        sys.argv = ['myapp.py']
        main()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('nterms', nargs='?', default=30, help='number of terms', type=int)

    args = parser.parse_args()

    # first two terms
    n1, n2 = 0, 1
    count = 0

    # check if the number of terms is valid
    if args.nterms <= 0:
        print("Please enter a positive integer")
    # if there is only one term, return n1
    elif args.nterms == 1:
        print("Fibonacci sequence upto", args.nterms, ":")
        print(n1)
    # generate fibonacci sequence
    else:
        print("Fibonacci sequence:")
        while count < args.nterms:
            print(n1)
            nth = n1 + n2
            # update values
            n1 = n2
            n2 = nth
            count += 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
