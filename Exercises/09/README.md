## Exercise 09

### Task 01: Run Mnist.py code in GPU partition

Mnist code uses tensorflow for hand written numbers classification. In this task, we need to run Mnist code on GPU partition using TensorFlow GPU libraries. We already have a Anaconda environment available with Tensorflow GPU installed.

Step 01: Create a `slurm-gpu.job` and add relevant SLURM directives. Make sure this time your job is submitted to GPU node. Hint: To request GPU, `--gres=gpu:1`

Step 02: In same file, Add `source /hps/software/opt/linux-rocky8-cascadelake/anaconda3/etc/profile.d/conda.sh` to enable Anaconda in your job

Step 03: In same file, Activate Anaconda Virtual Env which has Tensorflow installed. `conda activate tensorflow-gpu`

Step 04: Now add `python3 mnist.py` at end of your `slurm-gpu.job` to run your code.

Step 05: Submit your Job by issuing `sbatch slurm-gpu.job`

Step 05: Inspect results
