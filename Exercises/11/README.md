## Interactive HPC
  
### Task 01: Get familiar with ihpc portal

Instructions:

1. through your browser, head to https://ihpc.ebi.ac.uk
2. Check the files in your home directory. Choose one of them to download to your laptop.
3. Check your home directory quota
4. Open an interactive desktop session or a session with another application from the interactive apps menu