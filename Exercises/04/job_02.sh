#!/bin/bash
  
#SBATCH -J MatrixMul
#SBATCH --mem=8G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH -t 00:05:00
#SBATCH --reservation=training-oct-23
#SBATCH --account=training-oct-23
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-user=pkorir@ebi.ac.uk
#SBATCH --output=%j.out
#SBATCH --error=%j.err

module load openmpi
gcc -fopenmp matrix_mul.cpp
./a.out
