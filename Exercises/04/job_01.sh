#!/bin/bash

#SBATCH -J SleeperJob
#SBATCH --mem=2G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH -t 00:05:00
#SBATCH --reservation=training-oct-23
#SBATCH --account=training-oct-23

echo "This job does nothing and sleep for 30seconds"
sleep 30

